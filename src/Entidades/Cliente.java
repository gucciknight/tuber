/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import tuber.types.Client;
/**
 *
 * @author agustinrodriguez
 */
public abstract class Cliente extends FichaPosicion implements Client {
    String nombre;
    String numero;
    int idCliente;
    //variable de estado de cliente 0: desocupado 1: pidio uber 2 en viaje
    int state;
    
  //  boolean busy();
    public Cliente(String name, String number, float latC, float lonC, int id) {
     lat = latC;
     lon = lonC;
     nombre = name;
     idCliente = id;
     numero = number;
     symbol = 'C';
     state = 1;
    }
    @Override
    public boolean busy(){
        boolean r = false;
        switch (state) {
            case 0:
                r = false;
            case 1:
                r = false;
            case 2:
                r = true;
    } return r;
    
    }
    
    @Override 
    public boolean querying() {
        boolean r = false;
        switch (state) {
            case 0:
                r = false;
            case 1:
                r = true;
            case 2:
                r = false;
    } return r;
    }
}
        